// soal 1
console.log("LOOPING PERTAMA");
var angka = 2;
while (angka <= 20) {
    console.log(angka + " - I love coding");
    angka += 2;
}
console.log("LOOPING KEDUA");
var angka = 20
while (angka >= 2) {
    console.log(angka + " - I will become a frontend developer");
    angka -= 2;
}

// soal 2
for (var angka = 1; angka <= 20; angka ++) {
    if (angka % 2 == 0) {
        console.log( angka + " - Berkualitas");
    }
    else if (angka % 2 != 0 && angka % 3 != 0) {
        console.log( angka + " - Santai");
    }
    else {
        console.log( angka + " - I Love Coding");
    }
}

// soal 3
var k = "";
for (var i = 1; i <= 7; i ++) {
    for (var j = 1; j <= i; j ++) {
        k += "#";
    }
    k += "\n";
}
console.log(k);

// soal 4
var kalimat="saya sangat senang belajar javascript";
var hasil = kalimat.split(" ");
console.log (hasil);

// soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
var daftar1 = daftarBuah.toString().split(",").join("\n");
console.log (daftar1)



