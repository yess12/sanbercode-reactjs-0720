// soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var daftarPesertaObj = {
    nama : "Asep",
    "jenis kelamin" : "laki-laki",
    hobi : "baca buku",
    "tahun lahir" : 1992
}
console.log(daftarPesertaObj)

// soal 2
var buah = [
    {nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000},
    {nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000},
    {nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada", harga: 10000},
    {nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000}
]

console.log(buah[0])

// soal 3
var dataFilm = []
function tambahData(item){
    dataFilm.push(item)
}
var item = {
    nama: "The Maze Runner",
    durasi: "1 jam 53 menit",
    genre: "Science Fiction",
    tahun: "2014"
}
tambahData(item)
console.log(dataFilm)

// soal 4

//Release 0
class Animal {
    constructor(name, legs, cold_blooded) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
class Ape extends Animal {
    constructor(name, cold_blooded) {
        super(name, cold_blooded);
        this.legs = 2;
    }
    yell() {
        return console.log('Auooo')
    }
}
class Frog extends Animal {
    constructor(name, legs, cold_blooded) {
        super(name, legs, cold_blooded);
    }
    jump() {
        return console.log('hop hop')
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// soal 5
class Clock {
    constructor({template}) {

        var timer;
  
        function render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
    
        console.log(output);
        }
  
        this.stop = function() {
        clearInterval(timer);
        };
    
        this.start = function() {
        render();
        timer = setInterval(render, 1000);
        };
        }
          
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
