// soal 1
let luasLingkaran = (radius) => {
    return (22/7)*radius*radius;
}
var radius = 5;
console.log(luasLingkaran(radius));

const kelilingLingkaran = (rad) => {
    return 2*(22/7)*rad;
}
var rad = 3;
console.log(kelilingLingkaran(rad));

// soal 2
let kalimat = "";
var gabung = (first, second, third, fourth, fifth) => {
    return kalimat += `${first} ${second} ${third} ${fourth} ${fifth}`;
}
var first = 'saya'
var second = 'adalah'
var third = 'seorang'
var fourth = 'frontend'
var fifth = 'developer'
console.log(gabung(first, second, third, fourth, fifth));

// soal 3
class Book {
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}
class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price);
        this.isColorful = isColorful
    }
}

buku1 = new Book('Chemistry', 50, 45000);
console.log(buku1);
komik1 = new Komik('Doraemon', 25, 15000, false);
console.log(komik1);
komik2 = new Komik('Mewarnai', 15, 10000, true);
console.log(komik2);