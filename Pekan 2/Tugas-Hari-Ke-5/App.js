import React, { Component } from 'react';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: '',
      check1: false,
      check2: false,
      check3: false,
      check4: false,
      check5: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.onCheckChange = this.onCheckChange.bind(this);
  }

  handleChange(event){
    this.setState({
      value: event.target.value
    })
  }

  onCheckChange(e){
    console.log(e.target.checked);
    this.setState({
      [e.target.name]: e.target.checked
    })
  }

  render() {
    return (
      <div className="App" style={{
        width: "450px",
        border: "1px solid",
        borderRadius: "20px",
        padding: "20px",
        marginLeft: "20px",
        marginTop: "20px",
        fontFamily: "Times New Roman"
      }}>
        <div style={{textAlign: "center"}}>
          <h1>Form Pembelian Buah</h1>
        </div>
        <form>
          <b>Nama Pelanggan</b> <input style={{marginLeft: "35px"}} value={this.state.value} onChange={this.handleChange}/>
          <br />
          <br />
          <b>Daftar Item</b>
          <div style={{marginLeft: "80px", display: "inline-block"}}>
            <input type='checkbox' name='check1' checked={this.state.check1} onChange={this.onCheckChange}/> Semangka <br />
            <input type='checkbox' name='check2' checked={this.state.check2} onChange={this.onCheckChange}/> Jeruk <br />
            <input type='checkbox' name='check3' checked={this.state.check3} onChange={this.onCheckChange}/> Nanas <br />
            <input type='checkbox' name='check4' checked={this.state.check4} onChange={this.onCheckChange}/> Salak <br />
            <input type='checkbox' name='check5' checked={this.state.check5} onChange={this.onCheckChange}/> Anggur <br />
          </div>
          <br />
          <br />
          <input style={{borderRadius: "20px"}} type='submit' value='Kirim'/>
        </form>
      </div>
    );
  }
}

export default App;
